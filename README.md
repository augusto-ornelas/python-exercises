# Exercises

1. Calculate the number of binary ones in an integer value. A value can't be bigger than MAX_INT_64

 E.g. 'ones' is a name of function

  ones(3) -> 2

  ones(5) -> 2

  ones(0) -> 0

  ones(255) -> 8

  ones(256) -> 1

How to get complexity less than O(64)?

[Solution](./binary.py)

2. Rotate arbitrary string to the left/right to a specified number of position.

  E.g

   rotate('ABC', 'left', 1) -> 'BCA'

   rotate('ABC', 'right', 1) -> 'CAB'

[Solution](./rotate_string.py)

3. Implement a caching decorator which will return results of an arbitrary function from cache avoiding to call the decorated function when possible.
  It is known that decorated function always returns the same result if it's called with the same incoming parameters.
  Cache storage should be limited by specified number of items (N), and time parameter in seconds (TTL).
  That is, decorated function is not executed if a value that should be returned by the function is stored in the cache and its update time is less than specified TTL.
  On the other hand, if such a value is absent in the cache store, then it should be added keeping the total number of cache items limited by a parameter N.

```python
@cached(N=100, TTL=10)
def get(...)
```

[Solution](./cache.py)

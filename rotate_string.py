#  2. Rotate arbitrary string to the left/right to a specified number of position.
#  E.g
#   rotate('ABC', 'left', 1) -> 'BCA'
#   rotate('ABC', 'right', 1) -> 'CAB'

def rotate(s, direction, offset):
    len_s = len(s)
    if len_s == 0:
        return s
    offset %= len_s
    if direction == 'left':
        return s[offset:]+s[:offset]
    elif direction == 'right':
        return s[-offset:]+s[:-offset]
    else:
        # Do some error handling
        pass

# offset < len(s)
assert rotate('ABC', 'left', 1) == 'BCA'
assert rotate('ABC', 'right', 1) == 'CAB'
assert rotate('ABC', 'right', 2) == 'BCA'
assert rotate('ABCDE', 'right', 3) == 'CDEAB'
assert rotate('ABCDE', 'left', 3) == 'DEABC'
# offset > len(s)
assert rotate('ABC', 'right', 4) == 'CAB'
assert rotate('ABC', 'left', 4) == 'BCA'
assert rotate('ABC', 'left', 5) == 'CAB'
# empty string
assert rotate('', 'left', 1) == ''

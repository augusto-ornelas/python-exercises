# 3. Implement a caching decorator which will return results of an arbitrary
# function from cache, avoiding to call the decorated function when possible.

# It is known that the decorated function always returns the same result if it's
# called with the same incoming parameters.

# Cache storage should be limited by specified number of items (N), and time
# parameter in seconds (TTL).
# That is, the decorated function is not executed if a value that should be
# returned by the function is stored in the cache and its update time is less
# than specified TTL.
# On the other hand, if such a value is absent in the cache store, then it
# should be added keeping the total number of cache items limited by a parameter
# N.

# Answer: Since it was not specified in the exercise. I implemented Random
# Replacement as the policy of the cache.
import time
import random

def generate_hash(args, kwargs):
    key = args
    for item in kwargs.items():
        key += item
    return hash(key)

calls = 0
def cached(N, TTL):
    def wapper_func(user_function):
        cache = dict([])
        def wrapper(*args, **kwargs):
            global calls
            calls += 1
            key = generate_hash(args,kwargs)
            cached = cache.get(key)
            if cached is not None:
                if time.time() - cached['time']  >= TTL:
                    cache[key] = {'time': time.time(), 'result': user_function(*args,**kwargs)}
            elif len(cache) == N:
                key_to_rm = random.choice(list(cache.keys()))
                cache.pop(key_to_rm)
                cache[key] = {'time': time.time(), 'result': user_function(*args,**kwargs)}
            else:
                cache[key] = {'time': time.time(), 'result': user_function(*args,**kwargs)}
            return cache[key]['result']
        return wrapper
    return wapper_func

not_cached = 0
@cached(N=10, TTL=1)
def get(a,b,c,d):
    global not_cached
    not_cached +=1
    return a + b + c + d

number_of_tries = 8
for i in range(0,number_of_tries):
    for j in range(0,number_of_tries):
        time.sleep(0.5)
        get(i,i,i*20,i**3)
        get(a=j,c=j,b=j*2,d=i**7)

print(f"{calls} calls to `get`")
print(f"{calls-not_cached} cached")

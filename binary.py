# Calculate the number of binary ones in an integer value. A value can't be bigger than MAX_INT_64
#  E.g. 'ones' is a name of function
#   ones(3) -> 2
#   ones(5) -> 2
#   ones(0) -> 0
#   ones(255) -> 8
#   ones(256) -> 1

# How to get complexity less than O(64)?

# Answer:
# Knowing that n - 1 in binary returns n, but with the rightmost 1
# switched to a 0 and all the subsequent 0's turn into 1's:
# let's see an example:
# 44 = (101100)₂
# (101100 - 1)₂ = (101011)₂
# After, calculating the bit-wise AND: n & (n - 1)
# will return n with the rightmost 1 turned to 0
#     |--------------------|
#     v                    v
# (101100 & 101011)₂ = (101000)₂
# this allows us to count the number of 1's iterating one time per each 1 in `n`.
# The runtime of `ones` will be O(s) where `s` is the number of 1's in `n`
# and s < 64
def ones(n):
    count = 0
    while n > 0:
        n &= (n - 1) #remove rightmost 1
        count+=1
    return count

assert ones(3) == 2
assert ones(5) == 2
assert ones(0) == 0
assert ones(37) == 3
assert ones(255) == 8
assert ones(256) == 1
assert ones(44) == 3
assert ones(2**63-1) == 63
